package com.lukyan.example.elasticsearch.mapper;

import com.lukyan.example.elasticsearch.Application;
import com.lukyan.example.elasticsearch.dto.UserDto;
import com.lukyan.example.elasticsearch.jpa.entity.User;
import com.lukyan.example.elasticsearch.jpa.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@SpringBootTest(classes = Application.class)
class MapperTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ReferenceMapper referenceMapper;

    @Test
    void testUserMapperEntityToDto() {
        User entity = userRepository.findByUsername("john");
        UserDto dto = userMapper.asDto(entity);
        assertNotNull(dto);
    }

    @Test
    void testUserMapperDtoToEntity() {
        User entity = userRepository.findByUsername("john");
        UserDto dto = userMapper.asDto(entity);
        assertNotNull(userMapper.asEntity(dto));
    }

    @Test
    void testReferenceMapper() {
        User entity = userRepository.findByUsername("john");
        UserDto dto = userMapper.asDto(entity);
        assertNotNull(referenceMapper.resolve(dto, User.class));
    }
}
