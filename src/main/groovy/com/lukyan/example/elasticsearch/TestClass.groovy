package com.lukyan.example.elasticsearch

import groovy.transform.CompileStatic

@CompileStatic
class TestClass {
    String hello(){
        "Hello, World!!!"
    }
}
