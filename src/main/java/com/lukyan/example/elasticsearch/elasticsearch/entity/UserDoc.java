package com.lukyan.example.elasticsearch.elasticsearch.entity;

import com.lukyan.example.elasticsearch.common.IdHolder;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "userdoc")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDoc implements Serializable, IdHolder<Long> {

    private static final long serialVersionUID = -4454473971372851981L;

    @Id
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String username;

    @Field
    @Getter
    @Setter
    String fullName;
}
