package com.lukyan.example.elasticsearch.elasticsearch.repository;

import com.lukyan.example.elasticsearch.elasticsearch.entity.UserDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDocRepository extends ElasticsearchRepository<UserDoc, String> {
    List<UserDoc> findByFullName(String fullName);

    UserDoc findByUsername(String username);

    void deleteByUsername(String username);
}
