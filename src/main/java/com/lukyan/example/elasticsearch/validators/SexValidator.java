package com.lukyan.example.elasticsearch.validators;

import org.springframework.util.ObjectUtils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("sexValidator")
public class SexValidator implements Validator<String> {

    @Override
    public void validate(FacesContext context, UIComponent component,
                         String value) throws ValidatorException {
        if (ObjectUtils.isEmpty(value) || !("m".equals(value) || "f".equals(value))) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Incorrect input provided",
                    "The input must be m or f character"));
        }
    }
}