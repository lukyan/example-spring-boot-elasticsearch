package com.lukyan.example.elasticsearch.validators;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("noBlankSpaceValidator")
public class NoBlankSpaceValidator implements Validator<String> {

    @Override
    public void validate(FacesContext context, UIComponent component,
                         String value) throws ValidatorException {
        if (ObjectUtils.isEmpty(value)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Incorrect input provided",
                    "The input must provide some meaningful character"));
        }
    }
}