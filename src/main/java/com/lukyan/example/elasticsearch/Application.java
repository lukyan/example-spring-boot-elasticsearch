package com.lukyan.example.elasticsearch;

import com.lukyan.example.elasticsearch.elasticsearch.entity.UserDoc;
import com.lukyan.example.elasticsearch.elasticsearch.repository.UserDocRepository;
import com.lukyan.example.elasticsearch.jpa.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Slf4j
@AllArgsConstructor
@SpringBootApplication
public class Application implements CommandLineRunner {

    private final ElasticsearchOperations elasticsearchTemplate;
    private final UserDocRepository userDocRepository;
    private final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    //TODO move to elasticsearch migrations
    @Override
    public void run(String... args) {
        var testClass = new TestClass();
        log.debug(testClass.hello());

        userDocRepository.deleteAll();
        userRepository.findAll().stream().forEach(user -> {
            userDocRepository.save(UserDoc.builder().id(user.getId())
                    .username(user.getUsername()).fullName(user.getFullName()).build());
        });
        Iterable<UserDoc> userDocs = userDocRepository.findAll();
        if (log.isDebugEnabled()) {
            userDocs.forEach(userDoc -> log.debug("userDoc => id = {}, fullName = {}", userDoc.getId(),
                    userDoc.getFullName()));
        }
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchQuery("fullName", "John").minimumShouldMatch("75%")).build();
        SearchHits<UserDoc> userDocSearchHits =
                elasticsearchTemplate.search(searchQuery, UserDoc.class/*, IndexCoordinates.of("userDoc")*/);
        if (log.isDebugEnabled()) {
            userDocSearchHits.forEach(userDocSearchHit -> log.debug("userDocSearchHit => id = {}, fullName = {}", userDocSearchHit.getContent().getId(),
                    userDocSearchHit.getContent().getFullName()));
        }
    }
}
