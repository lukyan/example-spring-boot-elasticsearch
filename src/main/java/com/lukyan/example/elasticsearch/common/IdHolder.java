package com.lukyan.example.elasticsearch.common;

import java.io.Serializable;

public interface IdHolder<I extends Serializable> {
    I getId();

    void setId(I id);
}
