package com.lukyan.example.elasticsearch.jpa.entity;

import com.lukyan.example.elasticsearch.jpa.entity.converter.JPAInstantConverter;
import com.rits.cloning.Cloner;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User extends AbstractEntity<Long> {

    @Getter
    @Setter
    @Column(name = "full_name", nullable = false, length = 100)
    String fullName;

    @Getter
    @Setter
    @Column(name = "email", nullable = false, length = 100, unique = true)
    String email;

    @Getter
    @Setter
    @Column(name = "username", nullable = false, length = 50, unique = true)
    String username;

    @Getter
    @Setter
    @Convert(converter = JPAInstantConverter.class)
    @Column(name = "birthday", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    LocalDateTime birthday;

    @Getter
    @Setter
    @Column(name = "sex", nullable = false, length = 1)
    String sex;

    @Getter
    @Setter
    @Column(name = "photo")
    @Type(type = "org.hibernate.type.BinaryType")
    byte[] photo;

    @Getter
    @Setter
    @Column(name = "content_type", length = 10)
    String contentType;

    @Override
    protected User clone() {
        Cloner cloner = new Cloner();
        User cloned = cloner.deepClone(this);
        return cloned;
    }
}
