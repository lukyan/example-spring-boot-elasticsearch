package com.lukyan.example.elasticsearch.jpa.entity;

import com.lukyan.example.elasticsearch.common.IdHolder;
import com.lukyan.example.elasticsearch.jpa.entity.converter.JPAInstantConverter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public abstract class AbstractEntity<I extends Serializable> implements IdHolder<I> {

    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    I id;

    @Getter
    @Setter
    @Convert(converter = JPAInstantConverter.class)
    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    LocalDateTime createdAt;

    @Getter
    @Setter
    @Convert(converter = JPAInstantConverter.class)
    @Column(name = "updated_at", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
    LocalDateTime updatedAt;
}
