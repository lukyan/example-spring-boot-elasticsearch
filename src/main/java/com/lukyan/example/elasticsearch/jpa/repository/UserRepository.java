package com.lukyan.example.elasticsearch.jpa.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.lukyan.example.elasticsearch.jpa.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    void deleteById(Long id);
    List<User> findByUsernameIn(List<String> usernames);

    void deleteByUsername(String username);
}
