package com.lukyan.example.elasticsearch.mapper;

import com.lukyan.example.elasticsearch.dto.UserDto;
import com.lukyan.example.elasticsearch.jpa.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = ReferenceMapper.class)
public interface UserMapper extends GenericMapper<User, UserDto> {
    @Override
    @Mapping(target = "id")
    User asEntity(UserDto dto);
}