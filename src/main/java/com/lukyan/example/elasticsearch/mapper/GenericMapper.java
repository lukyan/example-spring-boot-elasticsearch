package com.lukyan.example.elasticsearch.mapper;

import java.util.List;

public interface GenericMapper<E, D> {
    E asEntity(D dto);

    D asDto(E entity);

    List<E> asEntityList(List<D> dtoList);

    List<D> asDtoList(List<E> entityList);
}