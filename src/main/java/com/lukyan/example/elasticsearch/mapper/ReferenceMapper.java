package com.lukyan.example.elasticsearch.mapper;

import com.lukyan.example.elasticsearch.common.IdHolder;
import com.lukyan.example.elasticsearch.jpa.entity.AbstractEntity;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.ObjectFactory;
import org.mapstruct.TargetType;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.lang.reflect.InvocationTargetException;

@Slf4j
@Component
public class ReferenceMapper {

    private final EntityManager em;

    public ReferenceMapper(EntityManager em) {
        this.em = em;
    }

    @ObjectFactory
    public <T extends AbstractEntity<?>> T resolve(IdHolder<?> idHolder,
                                                   @TargetType Class<T> type) {
        T entity = null;
        if (idHolder.getId() != null) entity = em.find(type, idHolder.getId());
        try {
            if (entity == null) {
                entity = type.getDeclaredConstructor().newInstance();
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            log.error(e.getMessage());
        }
        return entity;
    }
}