package com.lukyan.example.elasticsearch.bean;

import com.lukyan.example.elasticsearch.elasticsearch.entity.UserDoc;
import com.lukyan.example.elasticsearch.service.api.UserService;
import org.springframework.util.ObjectUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Named
@FacesConverter(value = "userDocConverter", managed = true)
public class UserDocConverter implements Converter<UserDoc> {

    private final UserService userService;

    @Inject
    public UserDocConverter(final UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDoc getAsObject(FacesContext fc, UIComponent uic, String value) {
        return ObjectUtils.isEmpty(value) ? null :
                userService.findDocByUsername(value);
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, UserDoc userDoc) {
        return ObjectUtils.isEmpty(userDoc) ? null :
                userDoc.getUsername();
    }
}