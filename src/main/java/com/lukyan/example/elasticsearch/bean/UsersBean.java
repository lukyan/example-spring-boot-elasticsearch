package com.lukyan.example.elasticsearch.bean;

import com.lukyan.example.elasticsearch.dto.UserDto;
import com.lukyan.example.elasticsearch.elasticsearch.entity.UserDoc;
import com.lukyan.example.elasticsearch.jpa.entity.User;
import com.lukyan.example.elasticsearch.service.api.UserService;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Named
@ViewScoped
public class UsersBean implements Serializable {

//    private static final long serialVersionUID = -80768090380440761L;

    private final UserService userService;

    private volatile List<UserDto> userDtos;

    @Getter
    @Setter
    private UserDto userDto;

    private StreamedContent photoStreamedContent;

    @Getter
    @Setter
    private UserDoc userDoc;

    @Getter
    @Setter
    private String searchString;

    @Inject
    public UsersBean(final UserService userService) {
        this.userService = userService;
    }

    @PostConstruct
    public void init() {
        this.userDtos = userService.findAll();
        //TODO this is workaround
        this.userDto = this.userDtos.get(0);
    }

    public StreamedContent getPhotoStreamedContent() {
        if (null != userDto && null != userDto.getPhoto()) {
            this.photoStreamedContent =
                    DefaultStreamedContent.builder()
                            .contentType(userDto.getContentType()).stream(() ->
                            new ByteArrayInputStream(userDto.getPhoto())).build();
            return photoStreamedContent;
        }
        return null;
    }

    public void setPhotoStreamedContent(StreamedContent streamedContent) throws IOException {
        this.photoStreamedContent = streamedContent;
        if (null != this.userDto) {
            this.userDto.setContentType(streamedContent.getContentType());
            this.userDto.setPhoto(streamedContent.getStream().readAllBytes());
        }
    }

    public List<UserDto> getUserDtos() {
        return this.userDtos;
    }

    public void delete(UserDto dto) {
        this.userService.delete(dto);
        this.userDtos.remove(userDto);
        FacesContext.getCurrentInstance().addMessage("messages",
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Success:", "Delete successful"));
    }

    public void showAddUserDialog() {
        this.userDto = new UserDto();
        LocalDateTime now = LocalDateTime.now();
        this.userDto.setCreatedAt(now);
        this.userDto.setUpdatedAt(now);
        PrimeFaces.current().executeScript("PF('addUserDialogVar').show();");
    }

    public void showEditUserDialog(@NonNull UserDto userParam) {
        this.userDto = userParam;
        PrimeFaces.current().executeScript("PF('addUserDialogVar').show();");
    }

    public void save() {
        try {
            if (null != this.userDto.getId()) {
                this.userDto.setUpdatedAt(LocalDateTime.now());
            }
            userDto = userService.save(userDto);
            userDtos = userService.findAll();
            FacesContext.getCurrentInstance().addMessage("messages",
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Success:", "Save successful"));
            PrimeFaces.current().executeScript("PF('addUserDialogVar').hide();");
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(), throwable);
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Store to database error with message:", throwable.getMessage()));
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        this.getUserDto().setPhoto(event.getFile().getContent());
    }

    public List<UserDoc> completeSearch(String query) {
        this.searchString = query;
        List<UserDoc> userDocs = userService.searchDocs(query);
        this.userDtos = userService.searchEntities(userDocs);
        PrimeFaces.current().ajax().update("usersForm:userList");
        return userDocs;
    }

    public void handleAutoCompleteItemSelect(SelectEvent<UserDoc> selectEvent) {
        UserDoc userDoc = selectEvent.getObject();
        UserDto dto = userService.findEntityByUsername(userDoc.getUsername());
        this.userDtos = Arrays.asList(new UserDto[]{dto});
    }
}