package com.lukyan.example.elasticsearch.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@NoArgsConstructor
public class UserDto extends AbstractDto<Long> {

    @Getter
    @Setter
    private String fullName;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime birthday;

    @Getter
    @Setter
    private String sex;

    @Getter
    @Setter
    private byte[] photo;

    @Getter
    @Setter
    private String contentType;
}