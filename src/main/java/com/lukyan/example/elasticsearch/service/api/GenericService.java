package com.lukyan.example.elasticsearch.service.api;

import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface GenericService<D, E, I> {

    D save(D dto);

    List<D> save(List<D> dtos);

    void deleteById(I id);

    Optional<D> findById(I id);

    List<D> findAll();

    List<D> findAll(Pageable pageable);

    //TODO may be rename to upsert or merge
    D update(E entity, I id);
}