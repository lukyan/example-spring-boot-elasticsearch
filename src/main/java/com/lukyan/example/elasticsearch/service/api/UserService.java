package com.lukyan.example.elasticsearch.service.api;

import com.lukyan.example.elasticsearch.dto.UserDto;
import com.lukyan.example.elasticsearch.elasticsearch.entity.UserDoc;
import com.lukyan.example.elasticsearch.jpa.entity.User;
import lombok.NonNull;

import java.util.List;

public interface UserService extends GenericService<UserDto, User, Long> {

    UserDto findEntityByUsername(String username);

    List<UserDoc> searchDocs(@NonNull String fullName);

    void delete(UserDto dto);

    UserDoc findDocByUsername(String username);

    List<UserDto> searchEntities(List<UserDoc> userDocs);
}
