package com.lukyan.example.elasticsearch.service.impl;

import com.lukyan.example.elasticsearch.dto.UserDto;
import com.lukyan.example.elasticsearch.elasticsearch.entity.UserDoc;
import com.lukyan.example.elasticsearch.elasticsearch.repository.UserDocRepository;
import com.lukyan.example.elasticsearch.jpa.entity.User;
import com.lukyan.example.elasticsearch.jpa.repository.UserRepository;
import com.lukyan.example.elasticsearch.mapper.UserMapper;
import com.lukyan.example.elasticsearch.service.api.UserService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.regexpQuery;

@Service("userService")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserDocRepository userDocRepository;
    private final ElasticsearchOperations elasticsearchTemplate;
    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository,
                           final UserDocRepository userDocRepository,
                           final ElasticsearchOperations elasticsearchTemplate,
                           final UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userDocRepository = userDocRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.userMapper = userMapper;
    }

    @Override
    public List<UserDto> findAll() {
        return userMapper.asDtoList(userRepository.findAll());
    }

    @Override
    public List<UserDto> findAll(Pageable pageable) {
        return userMapper.asDtoList(userRepository.findAll(pageable).getContent());
    }

    //TODO reimplement with 2-phase commit
    @Override
    @Transactional(propagation = Propagation.NESTED)
    public UserDto update(User entity, Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            User user = userRepository.save(entity);
            UserDoc userDoc = userDocRepository.findByUsername(user.getUsername());
            userDoc.setUsername(user.getUsername());
            userDoc.setFullName(user.getFullName());
            return userMapper.asDto(user);
        }
        return null;
    }

    //TODO reimplement with 2-phase commit
    @Override
    @Transactional(propagation = Propagation.NESTED)
    public void delete(@NonNull UserDto dto) {
        userDocRepository.deleteByUsername(dto.getUsername());
        userRepository.deleteByUsername(dto.getUsername());
    }

    //TODO reimplement with 2-phase commit
    @Override
    @Transactional(propagation = Propagation.NESTED) //??
    public UserDto save(UserDto dto) {
        User user = userMapper.asEntity(dto);
        userDocRepository.save(UserDoc.builder().id(user.getId())
                .username(user.getUsername()).fullName(user.getFullName()).build());
        return userMapper.asDto(userRepository.save(user));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public List<UserDto> save(List<UserDto> dtos) {
        return userMapper.asDtoList(userRepository
                .saveAll(userMapper.asEntityList(dtos)));
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<UserDto> findById(Long id) {
        Optional<User> optional = userRepository.findById(id);
        return Optional.of(optional.isPresent() ? userMapper.asDto(optional.get()) : null);
    }

    @Override
    public UserDoc findDocByUsername(String username) {
        return userDocRepository.findByUsername(username);
    }

    @Override
    public UserDto findEntityByUsername(String username) {
        return userMapper.asDto(userRepository.findByUsername(username));
    }

    @Override
    public List<UserDoc> searchDocs(String query) {
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        Query searchQuery = !ObjectUtils.isEmpty(query) && query.contains("*")
                ? queryBuilder.withQuery(regexpQuery("fullName", query)).build()
                : queryBuilder.withQuery(matchQuery("fullName", query)
                .minimumShouldMatch("75%")).build();
        return elasticsearchTemplate.search(searchQuery, UserDoc.class)
                .stream().map(SearchHit::getContent)
                .collect(Collectors.toList());
    }


    @Override
    public List<UserDto> searchEntities(List<UserDoc> userDocs) {
        return userDocs.stream().map(userDoc ->
                userMapper.asDto(userRepository.findByUsername(userDoc
                        .getUsername()))).collect(Collectors.toList());
    }
}
