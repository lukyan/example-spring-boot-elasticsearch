**Quick start**

clone repository

run
`docker-compose -f ./src/main/docker/docker-compose.yml up`
from project dir

run
`./mvnw clean spring-boot:run`
from project dir

open `http://localhost:8080` in your favorite browser

**NOTE:**
this requires jdk 16

